$('.js-link').click(function (event) {
    event.preventDefault();
    var elTo = $($(this).data('to'));
    $([document.documentElement, document.body]).animate({
        scrollTop: elTo.offset().top
    }, 1300 );
});

(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

ym(64609231, "init", {
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true
});
